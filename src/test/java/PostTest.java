import io.restassured.http.ContentType;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.*;
import static org.hamcrest.CoreMatchers.*;

public class PostTest extends BaseTest {

    @Test
    public void insereUsuario() {
        User user = new User(11, "Caio", "caiofn", "cfn@mail.com");
        given()
                .log().all()
                .contentType(ContentType.JSON)
                .body(user)
                .when()
                .post("/users")
                .then()
                .log().all()
                .statusCode(201)
                .contentType(ContentType.JSON)
                .body("id", is(11))
                .body("name", is("Caio"))
                .body("username", is("caiofn"))
                .body("email", is("cfn@mail.com"))

        ;

    }
}
