import io.restassured.http.ContentType;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.*;

public class PutTest extends BaseTest {
    private  static String url = "https://jsonplaceholder.typicode.com/todos/";

    @Test
    public void alteraTodos() {

        given().log().all()
                .contentType(ContentType.JSON)
                .relaxedHTTPSValidation()
                .body("{\n" +
                        "  \"userId\": 1,\n" +
                        "  \"id\": 1,\n" +
                        "  \"title\": \"caio\",\n" +
                        "  \"completed\": false\n" +
                        "}")
                .put(url+"1")
                .then()
                .statusCode(200)
                .body("title", is("caio"));


    }

    @Test
    public void alteraTodosInexistente() {

        given().log().all()
                .contentType(ContentType.JSON)
                .relaxedHTTPSValidation()
                .body("{\n" +
                        "  \"userId\": 1,\n" +
                        "  \"id\": 1,\n" +
                        "  \"title\": \"caio\",\n" +
                        "  \"completed\": false\n" +
                        "}")
                .put(url+"9999")
                .then()
                .statusCode(500);

    }
}
