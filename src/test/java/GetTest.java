import io.restassured.http.ContentType;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.*;
import static org.hamcrest.CoreMatchers.*;

public class GetTest extends BaseTest {

    @Test
    public void validarListaUsuarios() {
        given()
                .relaxedHTTPSValidation()
                .log().all()
                .when()
                .get("/users")
                .then()
                .statusCode(200)
                .contentType(ContentType.JSON)
                .log().all()
                .body("[0].name", equalTo("Leanne Graham"))
                .body("[0].address.street", equalTo("Kulas Light"))
                .body("[4].name", equalTo("Chelsey Dietrich"));
    }

    @Test(dataProvider = "dadosUsuarioEspecifico")
    public void validarUsuarioEspecifico(String id, String name) {
        given()
                .log()
                .all()
                .when()
                .get("/users/" + id)
                .then()
                .statusCode(200)
                .contentType(ContentType.JSON)
                .log().all()
                .body("name", is(name));

    }

    @Test
    public void validarUsuarioInexistente() {
        given()
                .log().all()
                .when()
                .get("/users/11")
                .then()
                .statusCode(404)
                .contentType(ContentType.JSON)
                .log()
                .all();
    }

    @DataProvider
    public Object[][] dadosUsuarioEspecifico() {
        return new Object[][]{
                {"1", "Leanne Graham"},
                {"2", "Ervin Howell"},
                {"3", "Clementine Bauch"},
                {"4", "Patricia Lebsack"},
                {"5", "Chelsey Dietrich"},
                {"6", "Mrs. Dennis Schulist"},
                {"7", "Kurtis Weissnat"},
                {"8", "Nicholas Runolfsdottir V"},
                {"9", "Glenna Reichert"},
                {"10", "Clementina DuBuque"}

        };
    }


}
