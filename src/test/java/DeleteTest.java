import io.restassured.http.ContentType;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;

public class DeleteTest extends BaseTest {

    @Test
    public void deletarUsuario() {

        given()
                .log().all()
                .when()
                .delete("/users/1")
                .then()
                .statusCode(200)
                .contentType(ContentType.JSON)
                .log().all();

    }

}
